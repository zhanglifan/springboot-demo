package com.example.demo.dao.entity;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>
 * 摄像头
 * </p>
 *
 * @author yangjun
 * @since 2023-05-10
 */
@Data
@TableName(autoResultMap = true)
@EqualsAndHashCode(callSuper = false)
public class CameraInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 摄像头编码
     */
    @TableId(value = "code", type = IdType.INPUT)
    private String code;

    /**
     * 扩展信息
     */
    @TableField(typeHandler = FastjsonTypeHandler.class)
    private JSONObject extra;

    /**
     * 摄像头名称
     */
    private String name;

    /**
     * 摄像头类型
     */
    private String cameraType;

    /**
     * 所属公司
     */
    private Integer companyId;

    /**
     * sbbh
     */
    private String sbbh;

    /**
     * vms的主设备id
     */
    private String vmsMainDeviceId;

    /**
     * 位置
     */
    private String location;

    /**
     * 经度
     */
    private BigDecimal longitude;

    /**
     * 维度
     */
    private BigDecimal latitude;

    private BigDecimal areaX;

    private BigDecimal areaY;

    /**
     * 摄像头描述
     */
    private String description;

    /**
     * 可能失联，可能被vms删除
     */
    private String enabled;

    /**
     * tag1
     */
    private String tag1;

    /**
     * tag2
     */
    private String tag2;

    /**
     * 是否推送
     */
    private String pushFlag;

    /**
     * 来源类型
     */
    private String sourceType;

    /**
     * 已删除
     */
    private Integer isDel;

    private Integer createBy;

    /**
     * 创建时间
     */
    private Date createTime;

    private Integer updateBy;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 备用1
     */
    private String backup1;

    /**
     * 备用2
     */
    private String backup2;

    /**
     * 取证地点描述
     */
    private String addressDesc;

    /**
     * 协议类型
     */
    private String agreementType;

    /**
     * ip地址
     */
    private String ipAddress;

    /**
     * 账号
     */
    private String account;

    /**
     * 密码
     */
    private String password;
}
