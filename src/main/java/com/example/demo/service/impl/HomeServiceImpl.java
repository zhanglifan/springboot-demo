package com.example.demo.service.impl;

import com.example.demo.dao.mapper.CameraInfoMapper;
import com.example.demo.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HomeServiceImpl implements HomeService {

    @Autowired
    private CameraInfoMapper cameraInfoMapper;

    @Override
    public String hello() {
        return "hello";
    }
}
