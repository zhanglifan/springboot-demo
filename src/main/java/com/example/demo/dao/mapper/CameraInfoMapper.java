package com.example.demo.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.dao.entity.CameraInfo;

/**
 * <p>
 * 摄像头 Mapper 接口
 * </p>
 *
 * @author yangjun
 * @since 2023-05-10
 */
public interface CameraInfoMapper extends BaseMapper<CameraInfo> {

}
