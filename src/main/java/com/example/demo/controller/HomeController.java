package com.example.demo.controller;
import org.apache.commons.codec.digest.DigestUtils;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.example.demo.dao.entity.CameraInfo;
import com.example.demo.dao.mapper.CameraInfoMapper;
import com.example.demo.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.StringHttpMessageConverter;
//import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Base64;

@Slf4j
@RestController
@RequestMapping("/home")
public class HomeController {

    @Autowired
    private HomeService homeService;
    @Autowired
    private CameraInfoMapper cameraInfoMapper;

    @GetMapping("/hello")
    public List<CameraInfo> hello() {
        List<CameraInfo> list = cameraInfoMapper.selectList(Wrappers.emptyWrapper());
        return list;
    }

    @PostMapping
    public List<CameraInfo> insertCamera(@RequestBody CameraInfo cameraInfo) {
        cameraInfoMapper.insert(cameraInfo);
        List<CameraInfo> list = cameraInfoMapper.selectList(Wrappers.emptyWrapper());
        return list;
    }

    //修改
    @PostMapping("/camerachange")
    public List<CameraInfo> updateCamera(@RequestBody CameraInfo cameraInfo) {
        cameraInfoMapper.update(cameraInfo, Wrappers.<CameraInfo>lambdaQuery().eq(CameraInfo::getCode, cameraInfo.getCode()));
        List<CameraInfo> list = cameraInfoMapper.selectList(Wrappers.emptyWrapper());
        return list;
    }

    @DeleteMapping("/camera/{id}")
    public void deleteById(@PathVariable("id") Serializable id) {
        cameraInfoMapper.deleteById(id);
    }

    /**
     * 推送单条短信
     *
     * @param
     */

    @RequestMapping(value = "/push", method = RequestMethod.POST)
    @ResponseBody
    public String push(@RequestBody SendReq req) {
        log.info(req.getEcName());
        log.info("req+++++++"+req);
//        StringBuffer stringBuffer = new StringBuffer();
//        stringBuffer.append("滁州市公安局交通巡逻警察支队")
//            .append("smai")
//            .append("Shanma123")
//            .append("18236994069")
//            .append("测试短信")
//            .append("7p9fM1uSt")
//            .append("");
//        System.out.println(stringBuffer);
//
//        String mdsMac = DigestUtils.md5Hex(stringBuffer.toString());
//        System.out.println("mds__________"+mdsMac);
//        req.setMac(mdsMac);

        //将传入的对象转换为json字符串
        String jsonString = JSONObject.toJSONString(req);
        String base64String = "";
        try {
            // 将 JSON 字符串转换为字节数组
            byte[] jsonBytes = jsonString.getBytes("UTF-8");
            // 使用 Base64 编码字节数组
            byte[] base64Bytes = Base64.getEncoder().encode(jsonBytes);
            // 将字节数组转换为字符串
            base64String = new String(base64Bytes, "UTF-8");
        } catch (Exception e) {
            e.printStackTrace();
        }

//        log.info(base64String);
        RestTemplate restTemplate = new RestTemplate();
        String response = restTemplate.postForObject("http://112.35.1.155:1992/sms/norsubmit", base64String, String.class);
        return response;
    }
}
