package com.example.demo.controller;
import java.util.Base64;
public class JsonBase64Converter {
    public static String jsonToBase64(String jsonString) { String base64String = "";
        try {
            // 将 JSON 字符串转换为字节数组
            byte[] jsonBytes = jsonString.getBytes("UTF-8");
            // 使用 Base64 编码字节数组
            byte[] base64Bytes = Base64.getEncoder().encode(jsonBytes);
            // 将字节数组转换为字符串
            base64String = new String(base64Bytes, "UTF-8"); } catch (Exception e) { e.printStackTrace(); }
        return base64String;
    }
    public static void main(String[] args) {
        String jsonString = "{\"name\": \"John\", \"age\": 30, \"city\": \"New York\"}";
        String base64String = jsonToBase64(jsonString);
        System.out.println("Base64 encoded JSON: " + base64String);
    }
}
